function addButtonVS() {
	// Si le bouton est déjà en place, pas la peine de le refaire
	var checkButtonVS = document.getElementById('VisualStudioPamButton');
	if(checkButtonVS != undefined)
		return;

	// Recherche de la barre de boutons
	// On cherche les éléments avec la bonne classe
	var rightButtonsElements = document.getElementsByClassName('project-right-buttons');

	// On en fait un array
	var arrayRightButtonsElements = Array.prototype.slice.call(rightButtonsElements);

	// On filtre les DIV
	var divsButtons = arrayRightButtonsElements.filter(function(elem){
												   return elem.nodeName == 'DIV';
												 });

	// Ajout d'un bouton
	// Page principale du projet
	if(divsButtons.length == 1) {
		// Récupération du bouton Download
		var divButtons = divsButtons[0];
		var buttons = divButtons.getElementsByClassName('btn has-tooltip');
		if(buttons.length == 1) {
			var downloadButton = buttons[0];

			// Récupération du lien pour cloner le repo
			var repoURL = document.getElementById('project_clone').getAttribute('value');
			repoURL = 'git-client://clone?repo=' + repoURL;

			// Initialisation d'un bouton VS sur la base du bouton DL
			var vsButton = downloadButton.cloneNode(true);
			vsButton.setAttribute('data-original-title', 'Open in Visual Studio');
			vsButton.setAttribute('title', 'Open in Visual Studio');
			vsButton.setAttribute('href', repoURL);
			vsButton.setAttribute('id', 'VisualStudioPamButton');
			vsButton.getElementsByTagName('i')[0].setAttribute('class', 'fa fa-windows');

			// Ajout du bouton avant celui de DL
			divButtons.insertBefore(vsButton, downloadButton);
		}
	}
	else { // On va vérifier si c'est la page des fichiers
		// Recherche de div.tree-controls, c'est là qu'est le bouton Download
		rightButtonsElements = document.getElementsByClassName('tree-controls');
		arrayRightButtonsElements = Array.prototype.slice.call(rightButtonsElements);
		divsButtons = arrayRightButtonsElements.filter(function(elem){
												   return elem.nodeName == 'DIV';
												 });
		if(divsButtons.length == 1) {
			var divButtons = divsButtons[0];

			// Ajout d'un élément dans la liste déroulante
			var dropMenu = divButtons.getElementsByClassName('dropdown-menu')[0];

			// Récupération du bouton de DL
			var dlButton = dropMenu.getElementsByTagName('li')[0];

			// Génération de l'URL du repo avec un 'hack' pour avoir l'URL absolue
			// Puisque celle dans les boutons existants est relative
			var repoURL = dlButton.getElementsByTagName('a')[0].getAttribute('href');
			var aURL = document.createElement('a');
			aURL.href = repoURL;
			repoURL = aURL.href;
			repoURL = 'git-client://clone?repo=' + repoURL.substr(0, repoURL.indexOf('/repository'));

			// Duplication d'un élément par simplicité
			var vsButton = dlButton.cloneNode(true);
			vsButton.setAttribute('id', 'VisualStudioPamButton');
			vsButton.getElementsByTagName('a')[0].setAttribute('href', repoURL);
			vsButton.getElementsByTagName('a')[0].getElementsByTagName('i')[0].setAttribute('class', 'fa fa-windows');
			vsButton.getElementsByTagName('a')[0].getElementsByTagName('span')[0].innerHTML = 'Open in Visual Studio';

			// Ajout du nouveau bouton
			dropMenu.insertBefore(vsButton, dlButton);
		}
	}
}

// Exécution de la fonction
addButtonVS();

// Ajout d'un Observer pour gérer les appels Ajax de Gitlab
var observer = new MutationObserver(function(mutations, observer) {
	// On limite un peu le déclenchement de la vérification
	var mutationContentChanged = mutations.filter(function(elt){
		return (elt.type === 'attributes') 
		&& (elt.attributeName === 'id');
	});

	// Si le changement observé remplit les conditions
	// On appelle la fonction qui se charge du reste
	if(mutationContentChanged.length > 0)
    	addButtonVS();
});

// Déclenchement de l'observation
observer.observe(document, {
  subtree: true,
  attributes: true
});